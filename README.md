# reverse_proxy

This is a basic config example of the reverse proxies used by jimber  
Please edit the `configs/nginx.conf` to your liking 

When you start up this environment for the first time there are no certificates,
this would mean that nginx won't start. But to generate a certificate we need nginx to start..  

Run `./init-letsencrypt.sh <domain_name>` to start up nginx with a dummy certificate and let the Certbot container ask a valid one for you.  
When adding more virtual hosts you should do these one by one by commenting out the `ssl_certificate` and `ssl_certificate_key` directives to make sure nginx will boot.

There is a little config file called `good-practices.conf`. This contains some SSL parameters and other best practices. It might be somewhat restrictive for older devices so use with caution.

After all is done simply run `docker-compose up -d` and your certificates will be auto renewed, if they are up for renewal, every day. :D
 
Seeing this?  
![cert.png](./cert.png)  

Edit `init-letsencrypt.sh` and set `staging=1` to `staging=0` if you're sure everything is setup correctly! You might run into letsencrypt requests limit otherwise.

This project is inspired by [@pentacent](https://medium.com/@pentacent/nginx-and-lets-encrypt-with-docker-in-less-than-5-minutes-b4b8a60d3a71)
